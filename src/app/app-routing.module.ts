import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ReservationComponent } from './pages/reservation/reservation.component';
import { PayComponent } from './pages/pay/pay.component';

const routes: Routes = [
  { path: '', component: ReservationComponent },
  { path: 'pay', component: PayComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
