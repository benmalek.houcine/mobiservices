import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';

import { Subject, map, takeUntil, tap } from 'rxjs';

import { MatDialog } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';

import { ReservationService } from 'src/app/services/reservation/reservation.service';

import { Reservation } from 'src/app/models/reservation';

import { DialogBookTicketComponent } from 'src/app/components/dialog-book-ticket/dialog-book-ticket.component';


@Component({
  selector: 'app-reservation',
  templateUrl: './reservation.component.html',
  styleUrls: ['./reservation.component.scss']
})
export class ReservationComponent implements OnInit, OnDestroy {
  noResult: boolean = false;
  noReservation: boolean = false;

  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'top';

  displayedColumns: string[] = ['leavingFrom', 'goingTo', 'date', 'time', 'price', 'action'];
  dataSource!: MatTableDataSource<Reservation>;
  dataSourceTemp: Reservation[] = [];

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  destroy$: Subject<void> = new Subject<void>();


  constructor(
    private dialog: MatDialog,
    private _snackBar: MatSnackBar,
    readonly reservationService: ReservationService
  ) {
  }

  ngOnInit(): void {

    this.reservationService.getReservations().pipe(
      takeUntil(this.destroy$),
      // tap(res => console.log('res : ', res)),
    ).subscribe({
      next: (res: any) => {
        this.dataSource = new MatTableDataSource(res);
        this.dataSourceTemp = res;
        this.reservationService.setNumberReservation(res.length);
        // this.numberReservation = res.length;

        (res.length == 0) ? this.noResult = true : this.noReservation = true;
      },
      error: err => {
        this._snackBar.open(err.message, 'Error message', {
          duration: 3000,
          horizontalPosition: this.horizontalPosition,
          verticalPosition: this.verticalPosition,
        });
      }
    });

  }

  addReservation() {
    const dialogRef = this.dialog.open(DialogBookTicketComponent, {
      width: '80%'
    });

    dialogRef.componentInstance.onBookRide.subscribe((newReservation) => {
      this.dataSourceTemp.push(newReservation);

      this.dataSource = new MatTableDataSource (this.dataSourceTemp);
      this.reservationService.incrementNumberRes();
      // this.numberReservation++;
    });
  }

  deleteReservation(id: number) {
    this.reservationService.deleteReservation(id).subscribe({
      next: (res: Reservation) => {
        this._snackBar.open('Votre reservation a été bien supprimée','', {
          duration: 3000,
          horizontalPosition: this.horizontalPosition,
          verticalPosition: this.verticalPosition,
        });

        this.dataSourceTemp = this.dataSourceTemp.filter((resev: Reservation) => resev.id != id );

        this.dataSource = new MatTableDataSource (this.dataSourceTemp);
        this.reservationService.decrementNumberRes();
        // this.numberReservation--;
      },
      error: (err) => {
        this._snackBar.open(err.message, 'Error message', {
          duration: 3000,
          horizontalPosition: this.horizontalPosition,
          verticalPosition: this.verticalPosition,
        });
      }
    })
  }

  editReservation(reserv: Reservation) {
    const dialogRef = this.dialog.open(DialogBookTicketComponent, {
      width: '80%',
      data: reserv
    });

    dialogRef.componentInstance.onEditResrvation.subscribe((reservationEdited) => {
      let indexToUpdate = this.dataSourceTemp.findIndex(reserv => reserv.id == reservationEdited.id);

      this.dataSourceTemp[indexToUpdate].leavingFrom = reservationEdited.leavingFrom;
      this.dataSourceTemp[indexToUpdate].goingTo = reservationEdited.goingTo;
      this.dataSourceTemp[indexToUpdate].price = reservationEdited.price;
      this.dataSourceTemp[indexToUpdate].date = reservationEdited.date;

      this.dataSource = new MatTableDataSource (this.dataSourceTemp);
    });
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

}
