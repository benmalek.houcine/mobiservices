import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

import { Subject, takeUntil, tap } from 'rxjs';

import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { MyErrorStateMatcher } from 'src/app/helpers/material-ui-functions';

import { Reservation } from 'src/app/models/reservation';

import { ReservationService } from 'src/app/services/reservation/reservation.service';
import { percentageDiscount } from 'src/app/helpers/functions';

type ModePayement = 'Paypal' | 'CB';

@Component({
  selector: 'app-pay',
  templateUrl: './pay.component.html',
  styleUrls: ['./pay.component.scss']
})
export class PayComponent implements OnInit {
  noResult: boolean = false;
  noReservation: boolean = false;
  total: number = 0;
  paypalDiscount: number = 0;

  panelCB = false;
  panelPaypal = false;
  paypalMail = new FormControl('', [Validators.required, Validators.email, Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]);
  matcher = new MyErrorStateMatcher();
  payCBForm!: FormGroup;

  @ViewChild('ccNumber') ccNumberField!: ElementRef;

  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'top';

  displayedColumns: string[] = ['leavingFrom', 'goingTo', 'date', 'time', 'price'];
  dataSource!: MatTableDataSource<Reservation>;
  dataSourceTemp: Reservation[] = [];

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  destroy$: Subject<void> = new Subject<void>();


  constructor(
    private fb: FormBuilder,
    readonly reservationService: ReservationService,
    private _snackBar: MatSnackBar,
  ) { }


  ngOnInit(): void {
    this.payCBForm = this.fb.group({
      cardNumber: ['', [Validators.required, Validators.pattern('^[ 0-9]*$'), Validators.minLength(17)]]
    });

    this.reservationService.getReservations().pipe(
      takeUntil(this.destroy$),
      tap(res => res.forEach(resev => this.total += resev.price)),
      // tap(res => console.log('res : ', res)),
    ).subscribe({
      next: (res: any) => {
        this.dataSource = new MatTableDataSource(res);
        this.dataSourceTemp = res;
        this.reservationService.setNumberReservation(res.length);
        this.paypalDiscount = percentageDiscount(this.total, 5);

        (res.length == 0) ? this.noResult = true : this.noReservation = true;
      },
      error: err => {
        this._snackBar.open(err.message, 'Error message', {
          duration: 3000,
          horizontalPosition: this.horizontalPosition,
          verticalPosition: this.verticalPosition,
        });
      }
    });

  }

  creditCardNumberSpacing(e: KeyboardEvent) : any {
    const keyCode = e.keyCode;
    // console.log('icii : ', keyCode);

    if (keyCode < 48 || keyCode > 57) {
      e.stopPropagation();
      e.preventDefault();
      e.returnValue = false;
      e.cancelBubble = true;
      return false;
    } else {
        const input = this.ccNumberField.nativeElement;
        const { selectionStart } = input;
        const { cardNumber } = this.payCBForm.controls;

        let trimmedCardNum = cardNumber.value.replace(/\s+/g, '');

        if (trimmedCardNum.length > 16) {
          trimmedCardNum = trimmedCardNum.substr(0, 16);
        }

        /* Handle American Express 4-6-5 spacing */
        const partitions = trimmedCardNum.startsWith('34') || trimmedCardNum.startsWith('37')
                          ? [4,6,5]
                          : [4,4,4,4];

        const numbers: any[] = [];
        let position = 0;
        partitions.forEach(partition => {
          const part = trimmedCardNum.substr(position, partition);
          if (part) numbers.push(part);
          position += partition;
        })

        cardNumber.setValue(numbers.join(' '));

        /* Handle caret position if user edits the number later */
        if (selectionStart < cardNumber.value.length - 1) {
          input.setSelectionRange(selectionStart, selectionStart, 'none');
        }
    }


  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

}
