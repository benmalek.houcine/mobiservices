import { Component, OnInit } from '@angular/core';
import { registerLocaleData } from '@angular/common';
import localeFr from '@angular/common/locales/fr';

import { ReservationService } from './services/reservation/reservation.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  numberReservation: number = 0;


  constructor(readonly reservationService: ReservationService) {
    registerLocaleData(localeFr);
  }

  ngOnInit(): void {
    this.reservationService.numberReservation$.subscribe((value) => {
      this.numberReservation = value;
    });
  }

}
