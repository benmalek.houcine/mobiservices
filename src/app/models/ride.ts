export interface Ride {
  id: number;
  leavingFrom: string;
  goingTo: string;
  date: number;
  price?: number;
}
