export interface Reservation {
  id: number;
  leavingFrom: string;
  goingTo: string;
  date: number;
  price: number
}
