import { Component, EventEmitter, Inject, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { Subject, map, takeUntil, tap } from 'rxjs';

import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

import { Ride } from 'src/app/models/ride';
import { Reservation } from 'src/app/models/reservation';

import { RidesService } from 'src/app/services/rides/rides.service';
import { ReservationService } from 'src/app/services/reservation/reservation.service';

type ActionBtn = 'Réserver' | 'Modifier';


@Component({
  selector: 'app-dialog-book-ticket',
  templateUrl: './dialog-book-ticket.component.html',
  styleUrls: ['./dialog-book-ticket.component.scss']
})
export class DialogBookTicketComponent implements OnInit, OnDestroy {
  minDate: Date = new Date();
  rideForm!: FormGroup;
  actionBtn: ActionBtn = 'Réserver';

  displayedColumns: string[] = ['leavingFrom', 'goingTo', 'date', 'time', 'price', 'book'];
  dataSource!: MatTableDataSource<Ride>;

  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'top';

  onBookRide = new EventEmitter();
  onEditResrvation = new EventEmitter();

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  destroy$: Subject<void> = new Subject<void>();


  constructor(
    private fb: FormBuilder,
    readonly ridesService: RidesService,
    readonly reservationService: ReservationService,
    private _snackBar: MatSnackBar,
    @Inject(MAT_DIALOG_DATA) public editData: Reservation,
    private dialogRef: MatDialogRef<DialogBookTicketComponent>
  ) {
  }

  ngOnInit(): void {
    this.rideForm = this.fb.group({
      leavingFrom: ['', Validators.required],
      goingTo: ['', Validators.required],
      date: ['', Validators.required],
    });

    // Si modification d'une réservation : on rempli le formulaire de recherche
    if (this.editData) {
      this.actionBtn = 'Modifier';

      let tmpEditData = {
        ...this.editData,
        date: new Date(this.editData.date)
      }
      this.rideForm.patchValue(tmpEditData);
    }
  }

  searchRide() {
    let rideFormDate = (this.rideForm.value.date).toDateString();

    this.ridesService.getRides().pipe(
      takeUntil(this.destroy$),
      map(elemts => elemts.filter((e: Ride) =>
        e.leavingFrom == this.rideForm.value.leavingFrom &&
        e.goingTo == this.rideForm.value.goingTo &&
        new Date(e.date).toDateString() == rideFormDate
      )),
      tap(res => this.dataSource = res),
    ).subscribe();
  }

  bookRide(row: Ride) {
    let reservation = <Reservation> {
      "leavingFrom": row.leavingFrom,
      "goingTo": row.goingTo,
      "date": row.date,
      "price": row.price
    };

    // Si un ajout d'une réservation
    if (!this.editData) {
      this.reservationService.postReservation(reservation).subscribe({
        next: (res) => {
          // On cas de succès, on affiche un message avec la SnackBar de Angular Material
          this._snackBar.open('Votre reservation pour ' + res.goingTo + ' est ajouté avec succès', 'Successfully', {
            duration: 3000,
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
          });
          this.onBookRide.emit(res);
        },
        error: (err) => {
          this._snackBar.open(err.message, 'Error message', {
            duration: 3000,
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
          });
        },
      });
    } else {
      // Si une modification
      this.reservationService.putReservation(reservation, this.editData.id).subscribe({
        next: (res) => {
          this._snackBar.open('Votre modification est effectué avec succès', 'Successfully', {
            duration: 3000,
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
          });
          this.onEditResrvation.emit(res);
          this.dialogRef.close();
        },
        error: (err) => {
          this._snackBar.open(err.message, 'Error message', {
            duration: 3000,
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
          });
        },
      });
    }

  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

}
