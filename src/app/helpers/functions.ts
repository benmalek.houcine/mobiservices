export function percentageDiscount(of: number, disc: number) {
 return of - (of * disc / 100);
}
