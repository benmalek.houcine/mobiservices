import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { BehaviorSubject, Observable } from 'rxjs';

import { Reservation } from 'src/app/models/reservation';

@Injectable({
  providedIn: 'root'
})
export class ReservationService {
  numberReservation$ = new BehaviorSubject<number>(0);


  constructor(private http: HttpClient) { }

  setNumberReservation(numberReservation: number) {
    this.numberReservation$.next(numberReservation);
  }

  incrementNumberRes() {
    let numberReservation = this.numberReservation$.getValue();
    this.numberReservation$.next(++numberReservation);
  }

  decrementNumberRes() {
    let numberReservation = this.numberReservation$.getValue();
    this.numberReservation$.next(--numberReservation);
  }

  getReservations(): Observable<Reservation[]> {
    return this.http.get<Reservation[]>('http://localhost:3000/reservations');
  }

  postReservation(reservation: Reservation): Observable<Reservation> {
    return this.http.post<Reservation>("http://localhost:3000/reservations", reservation);
  }

  deleteReservation(id: number): Observable<Reservation> {
    return this.http.delete<Reservation>("http://localhost:3000/reservations/" +  id);
  }

  putReservation(reservation: Reservation, id: number): Observable<Reservation> {
    return this.http.put<Reservation>("http://localhost:3000/reservations/" +  id, reservation);
  }

}
