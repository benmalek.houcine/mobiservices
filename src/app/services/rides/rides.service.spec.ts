import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';

import { RidesService } from './rides.service';

describe('RidesService', () => {
  let service: RidesService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule]
    });
    service = TestBed.inject(RidesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
